#!/bin/bash

# XYLT1 STR caller
# Repo: https://gitlab.com/andreassh/xylt1-str-caller

# Names or full paths of executable programs
bwa=bwa
samtools=samtools
expansionhunter=ExpansionHunter
reviewer=REViewer
python=python3

# Parameters
scriptdir=`dirname "$0"`
filename=$(basename -- "$1")
filename="${filename%.*}"
genome="hg38"
reference="$scriptdir/assets/xylt1_ref.fa"
motif=$(echo $2 | tr '[:lower:]' '[:upper:]')
keep=$3

# Variant catalogue selection
if [[ $motif = "GCC" ]]; then
    varcat="$scriptdir/assets/variant_catalog_GCCn.json"
elif [[ $motif = "TCCGCC" ]]; then
    varcat="$scriptdir/assets/variant_catalog_TCCnGCCn.json"
elif [[ $motif = "GCC-LONG" ]]; then
    varcat="$scriptdir/assets/variant_catalog_GCCn_offtargets.json"
elif [[ $motif = "TCCGCC-LONG" ]]; then
    varcat="$scriptdir/assets/variant_catalog_TCCnGCCn_offtargets.json"
else
    varcat="$scriptdir/assets/variant_catalog_TCCnGCCn.json"
fi

# Run the analysis
echo "::: Analysis has started :::";

# Extract regions from aligned file and create FASTQ
$python $scriptdir/assets/regions2fastq.py $1 $scriptdir/assets/regions2extract_$genome.bed $scriptdir/temp/

# Realign FASTQ onto new reference genome and index
$bwa mem -aM -A 1 -B 6 -O 4 $reference $scriptdir/temp/$filename.regions_R1.fq $scriptdir/temp/$filename.regions_R2.fq | \
$samtools sort - | \
$samtools view -Sb > $scriptdir/temp/$filename.bam
$samtools index $scriptdir/temp/$filename.bam

# Genotype with ExpansionHunter and create read visualisations
$expansionhunter --reads $scriptdir/temp/$filename.bam --reference $reference --variant-catalog $varcat --output-prefix $scriptdir/results/$filename
$samtools sort $scriptdir/results/${filename}_realigned.bam > $scriptdir/results/${filename}_realigned.sorted.bam ; samtools index $scriptdir/results/${filename}_realigned.sorted.bam
$reviewer --reads $scriptdir/results/${filename}_realigned.sorted.bam --reference $reference --catalog $varcat --locus XYLT1 --output-prefix $scriptdir/results/$filename --vcf $scriptdir/results/$filename.vcf
[ -f $scriptdir/results/$filename.XYLT1.svg ] && mv $scriptdir/results/$filename.XYLT1.svg $scriptdir/results/$filename.svg

# Move non-results files into temp/ folder
[ -f $scriptdir/results/${filename}_realigned.sorted.bam ] && mv $scriptdir/results/${filename}_realigned.sorted.bam $scriptdir/temp/${filename}_realigned.sorted.bam
[ -f $scriptdir/results/${filename}_realigned.sorted.bam.bai ] && mv $scriptdir/results/${filename}_realigned.sorted.bam.bai $scriptdir/temp/${filename}_realigned.sorted.bam.bai
[ -f $scriptdir/results/${filename}_realigned.bam ] && mv $scriptdir/results/${filename}_realigned.bam $scriptdir/temp/${filename}_realigned.bam
[ -f $scriptdir/results/${filename}.metrics.tsv ] && mv $scriptdir/results/${filename}.metrics.tsv $scriptdir/temp/${filename}.metrics.tsv
[ -f $scriptdir/results/${filename}.phasing.tsv ] && mv $scriptdir/results/${filename}.phasing.tsv $scriptdir/temp/${filename}.phasing.tsv

# If user has not asked to keep files, then delete them from temp/ folder
if [[ $keep != "keep" ]]; then
    rm $scriptdir/temp/$filename*
    echo "::: Intermediate files deleted :::";
fi

# If REViewer's output file is found, then analysis has completed successfully
if [[ -f "$scriptdir/results/$filename.svg" ]]; then
    echo "::: Analysis has finished :::";
    echo "::: Please see $scriptdir/results/ folder for $filename sample's SVG, JSON and VCF files :::"
else
    echo ">>> Error: Analysis failed to run successfully <<<"
fi