# XYLT1 STR caller
# This script here extracts out reads from BAM file that are in regions specified in the BED file and generates FASTQ files
# Stand-alone usage: python3 regions2fastq.py input_file.bam regions_file.bed output_folder/

import os
import sys
import csv
import regex
import pysam
from collections import defaultdict

def read_pair_generator(bam, region_coord, read_dict):
	for read in bam.fetch(region_coord[0], int(region_coord[1]), int(region_coord[2])):
		if not read.is_paired or read.is_secondary or read.is_supplementary:
			continue

		qname = read.query_name
		if qname not in read_dict:
			if read.is_read1:
				read_dict[qname][0] = read
			elif read.is_read2:
				read_dict[qname][1] = read
		else:
			if read.is_read1:
				yield read, read_dict[qname][1]
			elif read.is_read2:
				yield read_dict[qname][0], read
			del read_dict[qname]

def bam2fast(inputfile, regionsfile, outputfolder):
	bamfile = pysam.AlignmentFile(inputfile, "rb")
	bamfile_contigs = pysam.idxstats(inputfile)

	regions2extract = []
	with open(regionsfile) as regionsfile:                                                                                          
		lines = csv.reader(regionsfile, delimiter = '\t')
		for line in lines:
			if line[0] in bamfile_contigs:
				regions2extract.append(line)

	# Extract out reads and save as FASTQ
	fastq1_reads = []
	fastq2_reads = []

	read_dict = defaultdict(lambda: [None, None])
	for region in regions2extract:
		for read1, read2 in read_pair_generator(bamfile, region, read_dict):
			if read1 and read2:
				fastq1_reads.append(">%s\n%s\n+\n%s\n" % (read1.query_name, read1.query_sequence, "".join(map(lambda x: chr( x+33 ), read1.query_qualities))))
				fastq2_reads.append(">%s\n%s\n+\n%s\n" % (read2.query_name, read2.query_sequence, "".join(map(lambda x: chr( x+33 ), read2.query_qualities))))

	# Write FASTQ file
	inputfilename = os.path.splitext(os.path.basename(inputfile))[0]

	with open(outputfolder + inputfilename + ".regions_R1.fq", "w") as fastq_out:
		for r in fastq1_reads:
			fastq_out.write(r)

	with open(outputfolder + inputfilename + ".regions_R2.fq", "w") as fastq_out:
		for r in fastq2_reads:
			fastq_out.write(r)

if __name__ == '__main__':
	if len(sys.argv) >= 3:
		inputfilepath = sys.argv[1]
		regionsfilepath = sys.argv[2]
		outputfolderpath = sys.argv[3]
		bam2fast(inputfilepath, regionsfilepath, outputfolderpath)
	else:
		print("Missing arguments")
