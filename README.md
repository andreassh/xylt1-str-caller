# XYLT1 STR caller
Short tandem repeat (STR) expansion in XYLT1 promoter region is known to cause [Desbuquois dysplasia type 2](https://omim.org/entry/615777) ([LaCroix et al., 2019](https://www.cell.com/ajhg/fulltext/S0002-9297(18)30408-7)). However, the region where the STR expansion was discovered is not in the Human reference genome ([Faust, et al, 2014](https://doi.org/10.1186/s12863-014-0129-0)) and therefore it is difficult to genotype the locus from short-read sequencing data.

This simple script here provides a more accurate way to genotype the XYLT1 STR locus. In particular, it will extract out reads relevant to the locus which are then realigned to a [tiny decoy chromosome](./assets/xylt1_ref.fa) that contains section of the XYLT1 gene where we inserted the 238 bp reference-missing sequence and also another decoy for capturing fully repeated GCC reads. Finally, the realigned file will be genotyped by [ExpansionHunter](https://github.com/Illumina/ExpansionHunter) and read alignment visualisations created by [REViewer](https://github.com/Illumina/REViewer). Resulting files are VCF and JSON (genotyping results) and SVG (alignment visualisation image).

At the moment it works only with the hg38 genome. If you have any questions, problems, suggestions or successes of using it, then please [let me know](https://stripy.org/about#contact).

## Requirements
This method uses multiple programs together:
* [BWA](https://github.com/lh3/bwa)
* [Samtools](http://www.htslib.org/download/)
* [ExpansionHunter](https://github.com/Illumina/ExpansionHunter)
* [REViewer](https://github.com/Illumina/REViewer)
* [Python3](https://www.python.org/downloads/) with pysam library (to install for python, run: `python3 -m pip install pysam`).

NB! All tools have to be either in your `$PATH` (that is, you should be able to run them from command line just typing either `bwa`, `samtools`, `ExpansionHunter` or `REViewer`) or if not, then specify their full paths in the [call.sh](./call.sh) script (below the `# Names or full paths of executable programs` line).

## How to use
Clone the repository: `git clone https://gitlab.com/andreassh/xylt1-str-caller.git`

At first you might need to give executable rights to the script. To do that, execute `chmod +x call.sh`

If needed, configure the file paths of executable programs.

Run the ./call.sh script with the following options:
* Argument 1: input BAM file path
* Argument 2: targeted motif (either `TCCGCC` or only `GCC`), optionally specify `-LONG` in the end if you want to enable genotyping of long alleles that are over the fragment length (see examples below)
* Argument 3: specify "keep" to keep intermediate files in temp/ folder, otherwise leave empty

For example, to genotype either one real-life sample from 1000 Genomes, or simulated files (provided in examples/ folder), run:
* `./call.sh examples/real_HG00096.bam` ← genotypes both (TCC)n and (GCC)n repeats (default option)
* `./call.sh examples/real_HG00096.bam TCCGCC` ← genotypes both (TCC)n and (GCC)n repeats
* `./call.sh examples/real_HG00096.bam GCC` ← genotypes only (GCC)n repeats
* `./call.sh examples/simulated_300rep_het.bam GCC-LONG` ← enables genotyping of long expansions of (GCC)n
* `./call.sh examples/simulated_50rep_hom.bam TCCGCC-LONG` ← genotypes (TCC)n and enables genotyping of long expansions of (GCC)n

Temporary files will be stored into the script's temp/ folder and automatically deleted after the analysis. In order to keep them, specify `keep` as the **third** argument, such as: `./call.sh examples/real_HG00096.bam GCC keep`

Results will be saved into the script's results/ folder.
